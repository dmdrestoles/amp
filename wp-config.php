<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'amp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#aTe#r@CKsTDtD]<4cT~/O/U6pWGY`sm-IM})SP)_Lpp*DqH8HUI1?+EMAI4]Q`c' );
define( 'SECURE_AUTH_KEY',  'qP2|@n}&4gN>]g# zl:o7`sRX&g)@hK/Qmj=<k!Q_; O4_7ADzhqpKE(`##+6)U/' );
define( 'LOGGED_IN_KEY',    '&WG.[y;db1nDv.rW$D7K1iDuv4T>?I=[yqJVT/G_xmi5$I{rxY!k4vtrF9ez,K{u' );
define( 'NONCE_KEY',        '+cQA@cWB6;t!<wv=yK@lJX{9!{ZtKsy<|HVq6O}lS~2rY6f{HoO$jMkO?t9hG&{<' );
define( 'AUTH_SALT',        '5/EDU)Oh%}Q-g![J:-goB^DBfO-6)MLiz}D]Sr$}CF_FfzsZ&eBHSg5R%pS<Bvc{' );
define( 'SECURE_AUTH_SALT', 'L.E%6{;{v%(+K-T[`I]Jgc,RJLg2CkGy575ra^516Pu8,M!:/b~e)EV$^/=h9[Pu' );
define( 'LOGGED_IN_SALT',   '#}3vE ]|!~6~|xE8tKoF[A!$v:_uOSL`p9./Ls;lAqkhsp.Unqbh9%mkm9~nH[ZM' );
define( 'NONCE_SALT',       '7_XzTntFd$XX$ =eigk!.!GW{FagUHK.}[-iSF^iG3fC%OS9szMkP+$,q%dY|kO{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'amp';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
